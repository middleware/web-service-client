# Web Service Client Demo Project

This project provides and end-to-end demonstration for invoking Middleware
Web services according to best practices. It addresses the following:

* Client SSL for authentication
* Preparing the request
* Extracting data from the response
* Logging
* Testing

The following technologies/libraries are used:

* Spring-WS for interacting with messages in XML format
* Apache Axiom for SOAP marshalling
* Apache HttpComponents for HTTP messaging
* SLF4J for logging
* JUnit for unit testing

## Project Contents

    |-- pom.xml
    |-- README.md
    |-- src
    |   |-- main
    |   |   |-- java
    |   |   |   `-- edu
    |   |   |       `-- vt
    |   |   |           `-- middleware
    |   |   |               `-- ws
    |   |   |                   |-- AbstractWebServiceClient.java
    |   |   |                   `-- GroupQueryClient.java
    |   |   `-- resources
    |   |       |-- spring-config.xml
    |   |       |-- vt-truststore.jks
    |   |       `-- ws.properties
    |   `-- test
    |       |-- java
    |       |   `-- edu
    |       |       `-- vt
    |       |           `-- middleware
    |       |               `-- ws
    |       |                   `-- GroupQueryClientTest.java
    |       `-- resources
    |           `-- simplelogger.properties


## Configuration

A Java keystore named *edid.keystore* containing an ED Service certificate
and private key (i.e. keypair) must be located in the project root in order
to build and execute tests. The *ws.properties* file, which contains some
keystore configuration parameters, should be edited accordingly. We
recommend using a PKCS12 keystore (i.e. `edid.keystore.type=PKCS12`) since
there are a number of tools available to create and read PKCS12 files
(e.g. OpenSSL) and the format is supported natively by the Sun JSSE as of
1.4.


## Building


    mvn clean test


The output logs requests and responses by default, which are helpful for
learning as well as troubleshooting.

## Points of Study

* Spring Wiring (spring-config.xml)
* Basic Web service client features (AbstractWebServiceClient.java)
* Test methodology (GroupQueryClientTest.java)

It is expected that consumers of this demo would build their own clients
on top of AbstractWebServiceClient, e.g.

    public MyCustomWebServiceClient extends AbstractWebServiceClient {...}
