/*
  $Id: $

  Copyright (C) 2013 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.ws;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.dom.DOMResult;

import org.springframework.xml.xpath.NodeMapper;
import org.springframework.xml.xpath.XPathExpression;
import org.springframework.xml.xpath.XPathExpressionFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

/**
 * Demonstration SOAP Web service client for Middleware group query service,
 * https://apps.middleware.vt.edu/ws/ed/GroupQuery?wsdl
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public class GroupQueryClient extends AbstractWebServiceClient {

    /** Template request message for getMembers operation. */
    private static final String GET_MEMBERS_TEMPLATE = "" +
            "<q:getMembersRequest xmlns:q=\"http://middleware.vt.edu/schema/ed/query/\">\n" +
            "  <q:uugid>%s</q:uugid>\n" +
            "  <q:recursive>%s</q:recursive>\n" +
            "</q:getMembersRequest>";


    /** Soap action name for getMembers operation .*/
    private static final String GET_MEMBERS_ACTION = "http://middleware.vt.edu/getMembers";

    /** Compiled XPath expression to get group members. */
    private final XPathExpression xpGroups = XPathExpressionFactory.createXPathExpression(
            "//ed:group/ed:uugid",
            NS_MAP);

    /** Compiled XPath expression to get person members. */
    private final XPathExpression xpPersons = XPathExpressionFactory.createXPathExpression(
            "//ed:person/ed:uupid",
            NS_MAP);

    /**
     * Looks up the membership (persons and groups) of the given group.
     *
     * @param uugid Unique identifier of group to query for membership.
     * @param recursive Whether to drill down into the membership of contained groups.
     *
     * @return List of person or group member names.
     */
    public List<String> lookupMembers(final String uugid, final boolean recursive) {
        final DOMResult result = invoke(
                GET_MEMBERS_ACTION,
                String.format(GET_MEMBERS_TEMPLATE, uugid, recursive));

        final List<String> members = new ArrayList<String>();
        members.addAll(xpGroups.evaluate(
                result.getNode(),
                new NodeMapper<String>() {
                    public String mapNode(final Node node, final int i) throws DOMException {
                        return node.getTextContent();
                    }
                }));
        members.addAll(xpPersons.evaluate(
                result.getNode(),
                new NodeMapper<String>() {
                    public String mapNode(final Node node, final int i) throws DOMException {
                        return node.getTextContent();
                    }
                }));
        return members;
    }

}
