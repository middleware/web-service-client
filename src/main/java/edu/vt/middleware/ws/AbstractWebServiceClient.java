/*
  $Id: $

  Copyright (C) 2013 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.ws;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;
import org.w3c.dom.Node;

/**
 * Abstract base class for all Web service client classes.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
public abstract class AbstractWebServiceClient implements InitializingBean {

    /** Map of namespace prefixes to namespaces for use in XPath expressions. */
    protected static final Map<String, String> NS_MAP = new HashMap<String, String>(3);

    /** Logger instance. */
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /** Template for making Web service calls. */
    private WebServiceTemplate webServiceTemplate;

    /** XML transformer instance. */
    private Transformer transformer;

    /** Root URI to Web service endpoint. */
    private String endpointRootURI;

    /** Web service endpoint URI. */
    private String endpointURI;


    static {
        NS_MAP.put("ed", "http://middleware.vt.edu/schema/ed/");
        NS_MAP.put("q", "http://middleware.vt.edu/schema/ed/query/");
        NS_MAP.put("m", "http://middleware.vt.edu/schema/ed/manage/");
    }

    /**
     * Sets the Spring Web service template used for Web service invocations.
     *
     * @param template Web service template.
     */
    public void setWebServiceTemplate(final WebServiceTemplate template) {
        this.webServiceTemplate = template;
    }

    /**
     * Sets the root URI to Web service endpoints. Operation endpoint URIs are constructed
     * by appending to the root URI.
     *
     * @param rootURI  Root URI
     */
    public void setEndpointRootURI(final String rootURI) {
        this.endpointRootURI = rootURI;
    }

    public void afterPropertiesSet() throws Exception {
        final String c = getClass().getSimpleName();
        final StringBuilder sb = new StringBuilder(this.endpointRootURI);
        if (!this.endpointRootURI.endsWith("/")) {
            sb.append('/');
        }
        sb.append(c.substring(0, c.indexOf("Client")));
        this.endpointURI = sb.toString();
        this.transformer =  TransformerFactory.newInstance().newTransformer();
        this.transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        this.transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        this.transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    }

    protected DOMResult invoke(final String soapAction, final String message) {
        logger.debug("Sending message to {}:\n{}", this.endpointURI, message);
        final StreamSource source = new StreamSource(new StringReader(message));
        final DOMResult result = new DOMResult();
        final WebServiceMessageCallback callback = new WebServiceMessageCallback() {
            public void doWithMessage(final WebServiceMessage soapMessage) throws IOException, TransformerException {
                ((SoapMessage) soapMessage).setSoapAction(soapAction);
            }
        };
        if (this.webServiceTemplate.sendSourceAndReceiveToResult(this.endpointURI, source, callback, result)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Response message:\n{}", nodeToString(result.getNode()));
            }
            return result;
        }
        throw new IllegalStateException("No response returned for operation.");
    }

    protected String nodeToString(final Node node) {
        final StringWriter writer = new StringWriter();
        try {
            this.transformer.transform(new DOMSource(node), new StreamResult(writer));
        } catch (final TransformerException e) {
            logger.error("Error transforming node to string: " + e.getClass().getName() + "::" + e.getMessage());
        }
        return writer.getBuffer().toString();
    }
}
