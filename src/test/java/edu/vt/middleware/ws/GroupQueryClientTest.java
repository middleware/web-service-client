/*
  $Id: $

  Copyright (C) 2013 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: $
  Updated: $Date: $
*/
package edu.vt.middleware.ws;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for {@link GroupQueryClient} class.
 *
 * @author Middleware Services
 * @version $Revision: $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-config.xml")
public class GroupQueryClientTest {
    /** Logger instance. */
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private GroupQueryClient groupQueryClient;

    @Test
    public void testLookupMembers() throws Exception {
        final List<String> members = groupQueryClient.lookupMembers("middleware.sample.wstest", true);
        assertTrue(members.size() > 0);
        for (String member : members) {
            logger.debug("Got member {}", member);
            assertTrue(member.length() > 0);
        }
    }
}
